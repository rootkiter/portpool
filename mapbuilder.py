#!/bin/python
###############################################
# File Name : mapbuilder.py
#    Author : rootkiter
#    E-mail : rootkiter@rootkiter.com
#   Created : 2017-10-31 17:49:11 CST
###############################################

import sys,json,os,time,datetime

__UDP_PORT_LIST__ = [6666, 5060, 53413, 137, 123, 53, 161, 1200, 11211, 49, 2455, 9987, 1900, 520, 111, 389, 1434, 19, 20000, 2045, 47808, 9106, 9101, 60832, 2944, 5081, 10020, 10002, 10030, 626, 5070]
__TCP_PORT_LIST__ = [23, 1433, 6379, 445, 22, 5555, 3306, 2323, 80, 3389, 81, 8080, 5900, 5431, 443, 8181, 8090, 139, 8081, 135, 9090, 60001, 5901, 5902, 7070, 25, 7001, 8545, 2000, 27017, 1099, 21, 8089, 9000, 995, 110, 3128, 8118, 4899, 9600, 8999, 33891, 2433, 3390, 1444, 8082, 20000, 2222, 8040, 465, 1434, 8000, 2015, 8443, 8088, 1900, 63389, 137, 808, 7547, 2967, 138, 9200, 3392, 990, 102, 1080, 1025, 2082, 2220, 88, 2095, 111, 8888, 143, 2077, 2121, 8123, 89, 1911, 61202, 264, 800, 2010, 8890, 444, 389, 1723, 115, 8291, 113, 1023, 118, 119, 8010, 5000, 6666, 2012, 222, 194, 4786, 5223, 2375, 161, 33333, 8001, 9999, 52869, 123, 3127, 33892, 993, 8388, 109, 4444, 82, 179, 10001, 8022, 5432, 50070, 623, 10389, 2004, 51389, 11211, 8060, 2001, 47808, 1521, 4890, 502, 53, 8908, 35359, 85, 5060, 554, 5022, 9882, 8998, 9884, 8282, 65531, 8083, 4567, 2002, 4892, 9797, 2083, 7071, 10010, 37777, 9080, 3333, 20183, 5382, 7389, 10333, 50075, 1755, 11111, 3391, 16000, 5384, 5381, 3387, 10000, 2005, 5038, 17001, 1604, 9527, 60023, 13389, 1849, 2638, 1846, 2362, 5631, 8546, 953, 2376, 26881, 34567, 5380, 1848, 10002, 1002, 2200, 514, 515, 992, 1195, 636, 5357, 512, 16001, 2096, 33389, 2086, 5351, 30005, 7777, 1028, 520, 631, 5903, 6060, 5353, 888, 2380, 8073, 9922, 26020, 27018, 4040, 8983, 17000, 666, 9883, 2379, 1836, 63909, 2078, 60010, 5093, 3305, 4389, 3075, 13000, 6433, 49151, 9022, 37215, 9881, 33689, 9880, 53128, 60000, 1854, 1847, 4369, 4000, 1440, 6881, 6000, 789, 1000, 10086, 4687, 44818, 50050, 1111, 5001, 987, 18081, 33893, 8180, 8880, 23389, 50802, 3433, 4028, 3388, 18245, 2444, 1088, 30303, 1883, 53389, 50000, 8161, 15000, 199, 9300, 8008, 9443, 455, 259, 5984, 21443, 33899, 901, 873, 7002, 5500, 548, 38292, 1247, 783, 10051, 555, 1533, 15002, 3129, 1032, 63388, 27019, 13]

tcp_flame_format = "flame 'dst port %s proto TCP' -l 1000 | awk '{print($7)}'| grep \":\" | cut -d':' -f1 | sort -u"
udp_flame_format = "flame 'src port %s proto UDP' -l 1000 | awk '{print($5)}'| grep \":\" | cut -d':' -f1 | sort -u"

def run_cmd(cmd):
    return os.popen(cmd).read()

def getLastDayTimeWindow():
    timeNow = time.time()
    Last1Day = timeNow -24*60*60
    Last2Day = Last1Day-24*60*60

    rounding = lambda x:time.strptime(time.strftime("%Y-%m-%d 00:00:00", time.gmtime(x)),"%Y-%m-%d %H:%M:%S")
    accuracy = lambda x:int(time.mktime(rounding(x)))*1000
    return  accuracy(Last2Day), accuracy(Last1Day)

def dumpPortIp(proto,port):
    cmdstr = ""
    if(proto == 'udp'):
        cmdstr = udp_flame_format % str(port)
    elif(proto == 'tcp'):
        cmdstr = tcp_flame_format % str(port)
    else:
        return []

    runres = run_cmd(cmdstr).split()

    res = {}
    res["tmplist"] = runres
    res["worklist"] = []
    res["tmplen"] = str(len(runres))
    timestamp = time.time()
    res['timestamp'] = timestamp
    res["time"]   = time.strftime("%Y-%m-%d %H:%m:%S %Z", time.gmtime(timestamp))

    if(not os.path.exists('pool')):
        os.makedirs('pool')
    with open("pool/%s_%s.json" % (proto,str(port)), 'w') as f:
        f.write(json.dumps(res)+"\n")

    return len(runres)


def GetTcpPortList(url):
    return __TCP_PORT_LIST__
    #portlist = []
    #data = run_cmd("curl \""+url+"\"")
    #jsobj = json.loads(data)
    #for item in jsobj['top']:
    #    portlist.append(item['dstport'])
    #return portlist

if __name__=='__main__':
    timestart = datetime.datetime.now()
    last2day,last1day = getLastDayTimeWindow()

    i = 0
    for port in __UDP_PORT_LIST__:
        ipnum = dumpPortIp('udp',port)
        dtime = datetime.datetime.now() - timestart
        logstr = "%4d/%-4d %5s %5d %5d %20s" % (i+1,len(__UDP_PORT_LIST__),'udp',port,ipnum,dtime)
        print logstr
        i=i+1

    theURL = "http://netflow2.sec.bjsc.qihoo.net:8904/api/query/dstport_tops_stat/86400000/%d/%d/sum/?limit=300" % (last2day,last1day)
    tcp_port = GetTcpPortList(theURL)
    i = 0
    for port in tcp_port:
        ipnum = dumpPortIp('tcp',port)
        dtime = datetime.datetime.now() - timestart
        logstr = "%4d/%-4d %5s %5d %5d %20s" % (i+1,len(tcp_port),'tcp',port,ipnum,dtime)
        print logstr
        i=i+1

